require 'rake/testtask'
require 'rspec/core/rake_task'

ENV['LANG'] = 'C.UTF-8'

Rake::TestTask.new do |t|
  t.libs << 'test'
  t.test_files = FileList['test/**/*test.rb']
end

desc 'Run specs'
RSpec::Core::RakeTask.new

task default: [:test, :spec]
